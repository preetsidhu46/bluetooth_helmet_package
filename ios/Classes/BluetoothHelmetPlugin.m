#import "BluetoothHelmetPlugin.h"
#import <bluetooth_helmet/bluetooth_helmet-Swift.h>

@implementation BluetoothHelmetPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftBluetoothHelmetPlugin registerWithRegistrar:registrar];
}
@end
