package com.bounceshare.bluetooth_helmet.util;

public class MethodCalls {

   public interface BleHelmetScan {
        // Bluetooth helmet method calls
        String HELMET_SCAN = "bleHelmetScan";
        String START_PERIODIC_SCAN = "bleHelmetPeriodicScan";
        String STOP_PERIODIC_SCAN = "stopBleHelmetPeriodicScan";
        String HELMET_DATA_RECEIVED_METHOD = "helmetDataReceived";
        String HELMET_PERIODIC_SCAN_DATA_RECEIVED_METHOD = "periodicHelmetDataReceived";
    }

    public interface BluetoothState {
        // Bluetooth helmet method calls
        String BLE_OFF = "bleOff";
        String BLUETOOTH_TURNED_OFF = "bluetoothTurnedOff";
        String BLUETOOTH_TURNED_ON = "bluetoothTurnedOn";
        String BLUETOOTH_PERMISSION = "bluetoothPerm";
        String CHECK_BLUETOOTH_CONNECTION = "checkBleConnection";
    }

}
