package com.bounceshare.bluetooth_helmet;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.bounceshare.bluetooth_helmet.blescanner.BleDevicesScanner;
import com.bounceshare.bluetooth_helmet.blescanner.ScanDuration;
import com.bounceshare.bluetooth_helmet.blescanner.ScanInfoModel;
import com.bounceshare.bluetooth_helmet.blescanner.ScanRange;
import com.bounceshare.bluetooth_helmet.blescanner.ScanType;
import com.bounceshare.bluetooth_helmet.bluetooth.BluetoothStateUtil;
import com.bounceshare.bluetooth_helmet.bluetooth.IBluetoothStateChange;
import com.bounceshare.bluetooth_helmet.job_scheduler.HelmetScanUtil;
import com.bounceshare.bluetooth_helmet.util.MethodCalls;
import com.bounceshare.bluetooth_helmet.util.MethodChannels;
import com.google.gson.Gson;

import java.util.Map;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry.Registrar;
import io.reactivex.Single;
import io.reactivex.SingleSource;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;

/** BluetoothHelmetPlugin */
public class BluetoothHelmetPlugin implements MethodCallHandler {
  /** Plugin registration. */
  static MethodChannel methodChannel;

  static Activity activity;
  static boolean toAwait = false;
  static boolean toAwaitPerm = false;
  private static MethodChannel.Result mResult;
  private static MethodChannel.Result mResultPerm;
  public static void registerWith(Registrar registrar) {
    methodChannel = new MethodChannel(registrar.messenger(),  MethodChannels.HELMET_CHANNEL);
    methodChannel.setMethodCallHandler(new BluetoothHelmetPlugin());
    activity = registrar.activity();
  }
  private static IBluetoothStateChange iBluetoothStateChange = state -> {
    switch (state){
      case BT_ON:
        methodChannel.invokeMethod(MethodCalls.BluetoothState.BLUETOOTH_TURNED_ON, null);
        break;
      case BT_OFF:
        methodChannel.invokeMethod(MethodCalls.BluetoothState.BLUETOOTH_TURNED_OFF, null);
        break;
    }
  };
  @Override
  public void onMethodCall(MethodCall methodCallName, Result result) {
    String methodCall = methodCallName.method.trim();
    switch (methodCall) {
      case MethodCalls.BluetoothState.BLUETOOTH_PERMISSION:
        Map mapPerm = (Map) methodCallName.arguments;
        toAwaitPerm = (Boolean) mapPerm.get("toAwait");
        mResultPerm = result;
        BluetoothStateUtil.setBluetoothStateChange(toAwaitPerm ? null : iBluetoothStateChange);
        boolean status = BluetoothStateUtil.checkBluetoothPermission(activity);
        if (status || !toAwaitPerm)
          result.success(status);
        break;

      case MethodCalls.BleHelmetScan.HELMET_SCAN:
        Map map = (Map) methodCallName.arguments;
        Log.d("Map ==>>>>", "" + map.toString());
        toAwait = (Boolean) map.get("toAwait");
        mResult = result;
        Log.d("HELMET_SCAN", "======> Scan Initiated");
        RxJavaPlugins.setErrorHandler(Throwable::printStackTrace);
        new BleDevicesScanner(activity).scanItems(ScanRange.SHORT, ScanDuration.SHORT, ScanType.SINGLE_SCAN, null)
                .flatMap(BluetoothHelmetPlugin::saveScanInfoSingleScan)
                .subscribeOn(Schedulers.newThread())
                .subscribe();
        if (!toAwait)
          result.success(true);
        break;
      case MethodCalls.BleHelmetScan.START_PERIODIC_SCAN:
        Log.d("START_PERIODIC_SCAN", "======> Periodic Scan Initiated");
        HelmetScanUtil.setUpHelmetScanJob(activity);
        break;
      case MethodCalls.BleHelmetScan.STOP_PERIODIC_SCAN:
        Log.d("STOP_PERIODIC_SCAN", "======> Periodic Scan Stopping");
        HelmetScanUtil.stopHelmetScanJob(activity);
        break;
      default:
        result.notImplemented();
    }
  }
  /**
     * @param scanInfoModel
     * @return SingleSource<Boolean>
     *         <p>
     *         This method will call the Flutter method channel and pass the
     *         Periodic Scanned information (Contains the scanned helmet details
     *         along with API required values also)
     */
    public static SingleSource<Boolean> saveScanInfoSingleScan(ScanInfoModel scanInfoModel) {
      return Single.create(emitter -> {
        try {
          Log.d("Found Device Number===>", "" + scanInfoModel.getScannedDevices().length);
          new Handler(Looper.getMainLooper()).post(() -> {
            if (toAwait) {
              if (mResult != null) {
                mResult.success(getJsonData(scanInfoModel));
              }
            } else {
              if (methodChannel != null)
                methodChannel.invokeMethod(MethodCalls.BleHelmetScan.HELMET_DATA_RECEIVED_METHOD, getJsonData(scanInfoModel));
            }

            emitter.onSuccess(true);
          });
        } catch (Exception e) {
          e.printStackTrace();
          if (mResult != null) {
            mResult.success(null);
          }
          emitter.onError(e);
        }
      });

    }

    public static void handleAwaitPerm(boolean status) {
      try {
        if (mResultPerm != null)
          mResultPerm.success(BluetoothStateUtil.checkBluetoothPermission(activity));
        mResultPerm = null;
      }catch (IllegalStateException e){
        e.printStackTrace();
      }
    }

    public static void sendPeriodicData(ScanInfoModel scanInfoModel) {
      try {
        if (methodChannel != null)
          methodChannel.invokeMethod(MethodCalls.BleHelmetScan.HELMET_PERIODIC_SCAN_DATA_RECEIVED_METHOD, getJsonData(scanInfoModel));
      }catch (IllegalStateException e){
        e.printStackTrace();
      }

    }

    private static String getJsonData(ScanInfoModel scanInfoModel) {
      Gson gson = new Gson();
      return gson.toJson(scanInfoModel);
    }


}
