package com.bounceshare.bluetooth_helmet.bluetooth;

public interface IBluetoothStateChange {
    void currentBluetoothState(BluetoothState state);
}
