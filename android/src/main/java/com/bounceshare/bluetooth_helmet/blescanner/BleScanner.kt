package com.bounceshare.bluetooth_helmet.blescanner

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import android.location.LocationManager
import com.polidea.rxandroidble2.RxBleClient
import com.polidea.rxandroidble2.scan.ScanFilter
import com.polidea.rxandroidble2.scan.ScanSettings
import io.reactivex.Observable
import io.reactivex.functions.Function
import com.polidea.rxandroidble2.scan.ScanResult
import com.polidea.rxandroidble2.scan.ScanSettings.*
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import java.lang.Exception
import java.util.*
import java.util.concurrent.TimeUnit


class BleDevicesScanner(private val context: Context) : HelmetScanner {

    private val rxBleClient = RxBleClient.create(context)
    private lateinit var lastKnowLocation: Location;

    override fun scanItems(scanRange: ScanRange, scanDuration: ScanDuration, scanType: ScanType, helmetMacId: String?): Single<ScanInfoModel> {
        lastKnowLocation= getLocation();
        val scanId: String = UUID.randomUUID().toString()
        return Single.create { emitter ->
            val subscription = CompositeDisposable()
            lateinit var scan: Disposable
            val scanSub: Disposable = scanItemsInternal(scanId, scanRange, scanDuration, scanType).map {
                if (it.macId == helmetMacId) {
                    emitter.onSuccess(ScanInfoModel(scanId, arrayOf(it), lastKnowLocation?.latitude, lastKnowLocation?.longitude, System.currentTimeMillis(),getScanType(scanType)))
                    scan.dispose()
                    if (!subscription.isDisposed) {
                        subscription.clear()
                    }
                }
                it
            }
                    .buffer(secondsToScan(scanDuration), TimeUnit.SECONDS)
                    .subscribe({ scanInfo ->
                        val distinctScans: List<ScannedDevice> = scanInfo.distinctBy {
                            it.macId
                        }
                        emitter.onSuccess(ScanInfoModel(scanId, distinctScans.toTypedArray(), lastKnowLocation?.latitude, lastKnowLocation?.longitude, System.currentTimeMillis(),getScanType(scanType)))
                        scan.dispose()
                        if (!subscription.isDisposed) {
                            subscription.clear()
                        }

                    }, { t: Throwable? ->
                        emitter.onError(t ?: Exception())

                    })
            scan = scanSub
            subscription.add(scanSub)
        }
    }



    private fun scanItemsInternal(scanId: String, scanRange: ScanRange, scanDuration: ScanDuration, scanType: ScanType): Observable<ScannedDevice> {
        return rxBleClient
                .scanBleDevices(ScanSettings.Builder().setScanMode(SCAN_MODE_LOW_LATENCY).setCallbackType(CALLBACK_TYPE_ALL_MATCHES).build(),
                        ScanFilter.Builder().build())
//                .filter {
//                    it.rssi >= getRssiFilter()
//                }.filter {
//                    getNameRegex().toRegex().containsMatchIn(it?.bleDevice?.name?.toLowerCase()?: "")
//                }
                .map(mapToScannedDevice(scanId)).timeout(secondsToScan(scanDuration), TimeUnit.SECONDS, Observable.empty())

    }

    private fun getNameRegex(): String {
        return "";
       // return FirebaseRemoteConfig.getInstance().getString("bt_beacon_scan_name_filter")
    }

    private fun getRssiFilter(): Int {
        return 80
       // return FirebaseRemoteConfig.getInstance().getDouble("bt_beacon_scan_rssi_filter").toInt()
    }

    private fun getScanType(scanType: ScanType): String {
        return if(scanType== ScanType.PERIODIC) "periodic" else "trigger"
    }


    private fun secondsToScan(scanDuration: ScanDuration): Long {
        return if (scanDuration == ScanDuration.SHORT) 3 else 3
            //FirebaseRemoteConfig.getInstance().getLong("helmet_beacon_scan_seconds") else 10
    }

    private fun mapToScannedDevice(scanId: String): io.reactivex.functions.Function<in ScanResult, out ScannedDevice> {
        return Function {
           // Log.d("Device===> "+it.scanRecord?.deviceName, "scanned device mac: " + it.bleDevice.name + " address: ${it.bleDevice.macAddress}")
            val bleDevice = it.bleDevice
            ScannedDevice(scanId, System.currentTimeMillis(), bleDevice.macAddress, bleDevice.name
                    ?: "", it.rssi, getRssiFilter(), it.scanRecord.txPowerLevel, getLocation().latitude,null) //CoreApplication.getInstance(context).currentLocation()
        }

    }
    @SuppressLint("MissingPermission")
    private fun getLocation(): Location {
        val locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
    }
}