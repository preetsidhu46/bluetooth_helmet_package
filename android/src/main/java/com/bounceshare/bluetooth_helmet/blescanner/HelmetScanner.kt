package com.bounceshare.bluetooth_helmet.blescanner


import io.reactivex.Single

interface HelmetScanner {

    fun scanItems(scanRange: ScanRange, scanDuration: ScanDuration, scanType: ScanType, helmetMacId: String? = null): Single<ScanInfoModel>

}
enum class ScanRange {
    SHORT, LONG
}

enum class ScanDuration {
    SHORT, LONG
}