package com.bounceshare.bluetooth_helmet.blescanner

import androidx.annotation.Keep

@Keep
data class ScannedDevice(val scanId: String, val scanTime: Long, val macId: String, val name: String?, val rssi: Int, val rssiFilter: Int, val txLevel: Int?
                         , val scannedLat: Double?, val scannedLng: Double?)

@Keep
data class ScanInfoModel(val scanId: String, val scannedDevices: Array<ScannedDevice>, val scannedLat: Double?, val scannedLng: Double?, val scanTimeStamp: Long, val type: String) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ScanInfoModel

        if (scanId != other.scanId) return false
        if (!scannedDevices.contentEquals(other.scannedDevices)) return false
        if (scannedLat != other.scannedLat) return false
        if (scannedLng != other.scannedLng) return false
        if (scanTimeStamp != other.scanTimeStamp) return false
        if (type != other.type) return false

        return true
    }

    override fun hashCode(): Int {
        var result = scanId.hashCode()
        result = 31 * result + scannedDevices.contentHashCode()
        result = 31 * result + (scannedLat?.hashCode() ?: 0)
        result = 31 * result + (scannedLng?.hashCode() ?: 0)
        result = 31 * result + scanTimeStamp.hashCode()
        result = 31 * result + type.hashCode()
        return result
    }
}

enum class ScanType {
    PERIODIC, SINGLE_SCAN
}