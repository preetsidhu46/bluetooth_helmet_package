package com.bounceshare.bluetooth_helmet.job_scheduler;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.util.Log;


public class HelmetScanUtil {
    private static int JOB_ID = 115;

    /**
     * @param context Method used for schedule the Periodic Helmet Scan every 30 minutes
     */
    public static void setUpHelmetScanJob(Context context) {
        if (isJobServiceOn(context))
            return;
        JobScheduler jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        JobInfo jobInfo =new JobInfo.Builder(JOB_ID, new ComponentName(context, HelmetJobScheduler.class))
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                .setRequiresCharging(true)
                .setPeriodic(1000 * 60 * 30)//30 minutes
                .build();
        jobScheduler.schedule(jobInfo);
        Log.d("===JobScheduler===",""+jobScheduler.schedule(jobInfo));
    }

    public static void stopHelmetScanJob(Context context) {
        if (!isJobServiceOn(context))
            return;
        JobScheduler scheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        scheduler.cancel(JOB_ID);
    }


    /**
     * @param context
     * @return boolean
     * <p>
     * This method is for check the job scheduler is already scheduled or not. If we schedule it again then will replace any currently scheduled job
     * with the same ID with the new information in the JobInfo. If a job with the given ID is currently running, it will be stopped.
     */
    private static boolean isJobServiceOn(Context context) {
        JobScheduler scheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        boolean hasBeenScheduled = false;
        for (JobInfo jobInfo : scheduler.getAllPendingJobs()) {
            if (jobInfo.getId() == JOB_ID) {
                hasBeenScheduled = true;
                break;
            }
        }
        return hasBeenScheduled;
    }
}
