package com.bounceshare.bluetooth_helmet.job_scheduler;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;


import com.bounceshare.bluetooth_helmet.blescanner.BleDevicesScanner;
import com.bounceshare.bluetooth_helmet.BluetoothHelmetPlugin;
import com.bounceshare.bluetooth_helmet.blescanner.ScanDuration;
import com.bounceshare.bluetooth_helmet.blescanner.ScanInfoModel;
import com.bounceshare.bluetooth_helmet.blescanner.ScanRange;
import com.bounceshare.bluetooth_helmet.blescanner.ScanType;
import com.bounceshare.bluetooth_helmet.bluetooth.BluetoothStateUtil;

import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.SingleSource;
import io.reactivex.schedulers.Schedulers;

public class HelmetJobScheduler extends JobService {

    @Override
    public boolean onStartJob(JobParameters params) {
        try {
            if (BluetoothStateUtil.isBluetoothEnabled())
                new BleDevicesScanner(this).scanItems(ScanRange.SHORT, ScanDuration.SHORT, ScanType.PERIODIC, null)
                        .flatMap(HelmetJobScheduler::saveScanInfo)
                        .subscribeOn(Schedulers.newThread())
                        .subscribe();
        }catch (IllegalStateException e){
            e.printStackTrace();
        }
            return true;

    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return false;
    }

    /**
     * @param scanInfoModel
     * @return SingleSource<Boolean>
     * <p>
     * This method will call the Flutter method channel and pass the Periodic Scanned information (Contains the scanned helmet details along with API required values also)
     */
    public static SingleSource<Boolean> saveScanInfo(final ScanInfoModel scanInfoModel) {
        return Single.create(new SingleOnSubscribe<Boolean>() {
            @Override
            public void subscribe(final SingleEmitter<Boolean> emitter) throws Exception {
                try {
                    Log.d("Found Device Number===>", "" + scanInfoModel.getScannedDevices().length);
                    new Handler(Looper.getMainLooper()).post(
                            new Runnable() {
                                @Override
                                public void run() {
                                    BluetoothHelmetPlugin.sendPeriodicData(scanInfoModel);
                                    emitter.onSuccess(true);
                                }
                            });
                } catch (Exception e) {
                    e.printStackTrace();
                    emitter.onError(e);
                }
            }
        });
    }
}
