import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

class BluetoothHelmet {
  static const MethodChannel _channel =
      const MethodChannel('com.hawkeye.blehelmet');
  Function methodCallbackFunction;
  BluetoothHelmet({@required Function callback}) {
    methodCallbackFunction = callback;
    _channel.setMethodCallHandler((m) => setupMethodHandler(m));
  }
  setupMethodHandler(m) async {
    String channelName = m.method.trim();
    print("channelName =====> $channelName");

    switch (channelName) {
      case "bluetoothTurnedOn":
        print('BTchannel on');
        methodCallbackFunction(methodName: channelName);
        break;
      case "bluetoothTurnedOff":
        print('BTchannel off');
        methodCallbackFunction(methodName: channelName);
        break;
      case "helmetDataReceived":
        dynamic scanModel = m.arguments;
        print("Got Data ScanModel");
        methodCallbackFunction(
            methodName: channelName,
            arguments: scanModel,
            type: ScanType.TRIGGER);
        break;
      case "periodicHelmetDataReceived":
        dynamic scanModel = m.arguments;
        print("Got Data ScanModel");
        methodCallbackFunction(
            methodName: channelName,
            arguments: scanModel,
            type: ScanType.PERIODIC);
        break;

      default:
        print('BTchannel no method');
    }
  }

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  scanBleHelmet({int duration = 3, bool toAwait = false}) async {
    bool status = await permissionBle(toAwait: toAwait);
    if (status) {
      Map argument = {'duration': duration, 'toAwait': toAwait};
      dynamic scanModel =
          await _channel.invokeMethod("bleHelmetScan", argument);
      print('scanBleHelmet==>>> $scanModel');
      return scanModel;
    }
    return null;
  }

  /// Expecting arugment duration(In Seconds)
  periodicHelmetScan({int duration = 3}) async {
    bool status = await permissionBle();
    if (status) {
      Map argument = {'duration': duration};
      bool status =
          await _channel.invokeMethod("bleHelmetPeriodicScan", argument);
      print('periodic Scan==>>> $status');
    }
  }

  stopPeriodicHelmetScan() async {
    bool status = await _channel.invokeMethod("stopBleHelmetPeriodicScan");
    print('stop periodic Scan==>>> $status');
  }

  permissionBle({bool toAwait = false}) async {
    Map argument = {'toAwait': toAwait};
    bool perm = await _channel.invokeMethod('bluetoothPerm', argument);
    print('prem==>>> $perm');
    return perm;
  }

  Future<bool> checkBle() async {
    bool check = await _channel.invokeMethod('checkBleConnection');
    print('BTchannel check: $check');
    return check;
  }
}

enum ScanType { TRIGGER, PERIODIC }
