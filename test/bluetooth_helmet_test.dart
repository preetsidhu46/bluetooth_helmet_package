import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:bluetooth_helmet/bluetooth_helmet.dart';

void main() {
  const MethodChannel channel = MethodChannel('bluetooth_helmet');

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await BluetoothHelmet.platformVersion, '42');
  });
}
